# -*- coding: utf-8 -*-
import vk
import re
from pymongo import MongoClient


class VkDB():
    """ Обращения к базе данных (mongo). """

    def __init__(self, mongo_ip, mongo_port):
        """ Инициализация mongo. """
        mongo = MongoClient(mongo_ip, mongo_port, connect=False)
        vk_db = mongo.vk
        self.vk_account = vk_db.vk_account

    def write_vk_accounts_into_text_file(self):
        """ Сохранение данных из БД в файл 'vk_members.txt'. """
        filename = 'vk_members.txt'
        delimiter = '=' * 20
        empty_value = '-' * 4
        persons = self.vk_account.find()
        with open(filename, 'a') as f:
            for i, person in  enumerate(persons, 1):
                f.writelines('{}{}\n'.format(i, delimiter))
                for field in person:
                    if 'id' in field:
                        continue
                    elif 'career' in field:
                        value = [job['position']
                                 for job in person.get(field, {})
                                 if 'position' in job] or empty_value
                    else:
                        value = person.get(field, empty_value)
                    element_to_write = f'{field}: {value}\n'
                    f.writelines(element_to_write)

    def save_accounts(self, accounts):
        """ Сохранение перечня описания аккаунтов в бд.

        Ряд полей описания игнорируется при сохранении.

        :param iterable[dict] accounts: каждый элемент описывает параметры аккаунта vk.
        :rtype: None
        """
        unnecessary_fields = ('is_closed', 'can_access_closed', 'university', 'faculty')
        for person in accounts:
            is_person_present = self.vk_account.find_one({"id": person.get("id", "")})
            if is_person_present:
                continue
            else:
                updated_person = person.copy()
                for field in unnecessary_fields:
                    try:
                        updated_person.pop(field, None)
                    except:
                        pass
                city = updated_person.pop("city").get("title", "")
                updated_person["city"] = city
                print(updated_person)
                self.vk_account.save(updated_person)


class VkAPI():
    """ Обращения к vk.api. """

    def __init__(self, api_version, access_token, group_access_token):
        """ Сохранение параметров для обращения к vk.api. """
        self.access_token = access_token
        self.group_access_token = group_access_token
        self.vk_api_version = api_version
    
    @staticmethod
    def filter_accounts(accounts, filters=[], deserved_city='Moscow'):
        """ Фильтр аккаунтов (описаний их параметров) по заданным критериям и по городу (Москва).
        
        :param iterable[dict] accounts: каждый элемент соответствует описанию аккаунта
        :param iterable[str] filters: поля фильтрации, хотя бы одно должно быть заполнено
        :param str deserved_city: игнорировать аккаунты не из указанного города
        :return: описание параметров аккаунта vk
        :rtype: generator[dict]
        """
        def is_person_from_city(person, deserved_city):
            """ True, если городом числится указанный город. """
            person_city = person.get('city', {}).get('title', '')
            return person_city == deserved_city
        
        def is_field_value_valid(person, field_name):
            """ True, если значение поля содержит хотя бы 7 цифр. """
            field_value = person.get(field_name, '')
            digits = re.findall(r'\d', field_value)
            return len(digits) >= 7 and '*' not in field_value
        
        def does_person_pass_filters(person, filters):
            """ True, если хотя бы одно из полей корректно. """
            do_filters_pass = (is_field_value_valid(person, field_name)
                               for field_name in filters)
            return any(do_filters_pass)
                    
        for person in accounts:
            if not is_person_from_city(person, deserved_city):
                continue
            elif filters and not does_person_pass_filters(person, filters):
                continue
            else:
                yield person

    def get_group_members(self,
                          group_id,
                          fields="career, city, connections, contacts, education"):
        """ Генератор объектов, описывающих аккаунт vk по заданным параметром.
        
        :param str group_id: id группы, например "bitrix_24_official"
        :param str fields: параметры описания, через запятую
          Available values: sex, bdate, city, country, photo_50, photo_100,
            photo_200_orig, photo_200, photo_400_orig, photo_max, photo_max_orig,
            online, online_mobile, lists, domain, has_mobile, contacts, connections,
            site, education, universities, schools, can_post, can_see_all_posts,
            can_see_audio, can_write_private_message, status, last_seen, common_count,
            relation, relatives, counters. 
        :return: описание параметров аккаунта vk
        :rtype: generator[dict]
        """
        session = vk.Session(access_token=self.access_token)
        vk_api = vk.API(session)
        group_members_response = vk_api.groups.getMembers(group_id=group_id,
                                                          fields=fields,
                                                          v=self.vk_api_version)
        group_members = group_members_response.get('items', [])
        for person in group_members:
            yield person

    def get_groups(self, search_string):
        """ Генератор id групп из поиска по ключевому слову.
        
        :param str search_string: ключевое слово для поиска
        :return: id группы или пустая строка
        :rtype: generator[str] 
        """
        session = vk.Session(access_token=self.group_access_token)
        vk_api = vk.API(session)
        groups_response = vk_api.groups.search(q=search_string,
                                               v=self.vk_api_version,
                                               count=1000)
        groups = groups_response.get('items', [])
        for group in groups:
            yield group.get('id', '')


def main_scenario(vk_api, vk_db, group_search_strings, necessary_fields):
    """ Действия для поиска аккаунтов, сохранения в бд и записи в файл. """
    for search_string in group_search_strings:
        groups = vk_api.get_groups(search_string)
        for group_id in groups:
            group_memebers = vk_api.get_group_members(group_id)
            valid_group_memebers = vk_api.filter_accounts(group_memebers,
                                                          filters=necessary_fields,
                                                          deserved_city="Moscow")
            vk_db.save_accounts(valid_group_memebers)
    vk_db.write_vk_accounts_into_text_file()


## параметры для vk.api и для mongo:
VK_API_SETTINGS = dict(
    api_version=5.92,
    access_token="",
    group_access_token=""
)
VK_DB_SETTINGS = dict(
    mongo_ip="127.0.0.1",
    mongo_port=27017,
)
# перечень полей контактов аккаунта, хотя бы одно из котороых обязательно; например
#   'mobile_phone'  поле для мобильного
#   'home_phone'    поле для домашнего
#   'skype'         поле для skype
#   'twitter'       поле для twitter
#   'instagram'     поле для instagram
NECESSARY_ACCOUNT_FIELDS = ('mobile_phone', 'home_phone')
GROUP_SEARCH_STRINGS = ("битрикс", "битрикс24", "bitrix24", "bitrix")  # перечень строк-запросов для поиска


if __name__ == "__main__":
    vk_api = VkAPI(**VK_API_SETTINGS)
    vk_db = VkDB(**VK_DB_SETTINGS)
    main_scenario(vk_api, vk_db, GROUP_SEARCH_STRINGS, NECESSARY_ACCOUNT_FIELDS)
